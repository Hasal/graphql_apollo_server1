// https://neo4j.com/docs/graphql-manual/current/getting-started/
// Neo4j GraphQL Library

const { Neo4jGraphQL } = require("@neo4j/graphql");
const { ApolloServer, gql } = require("apollo-server");
const neo4j = require("neo4j-driver");

// define type definitions or schema
const typeDefs = gql`
    type FilterGroup {
	    filterStatementsBelongsTo: [FilterStatement!]! @relationship(type: "BELONGS_TO", direction: IN)
	    name: String!
    }

    type FilterStatement {
	    belongsToFilterGroups: [FilterGroup!]! @relationship(type: "BELONGS_TO", direction: OUT)
	    qualityFactorsBelongsTo: [QualityFactor!]! @relationship(type: "BELONGS_TO", direction: IN)
	    qualityFactorsRelevantFor: [QualityFactor!]! @relationship(type: "RELEVANT_FOR", direction: IN)
	    text: String!
    }

    type LifeCyclePhase {
	    name: String!
	    qualityCharacteristicsContributesTo: [QualityCharacteristic!]! @relationship(type: "CONTRIBUTES_TO", direction: IN)
    }

    type QualityCharacteristic {
        contributesToLifeCyclePhases: [LifeCyclePhase!]! @relationship(type: "CONTRIBUTES_TO", direction: OUT)
        description: String!
        name: String!
        qualityFactorsContributesTo: [QualityFactor!]! @relationship(type: "CONTRIBUTES_TO", direction: IN)
        qualityFactorsRelevantFor: [QualityFactor!]! @relationship(type: "RELEVANT_FOR", direction: IN)
    }

    type QualityFactor {
        belongsToFilterStatements: [FilterStatement!]! @relationship(type: "BELONGS_TO", direction: OUT)
        contributesToQualityCharacteristics: [QualityCharacteristic!]! @relationship(type: "CONTRIBUTES_TO", direction: OUT)
        description: String!
        name: String!
        relevantForFilterStatements: [FilterStatement!]! @relationship(type: "RELEVANT_FOR", direction: OUT)
        relevantForQualityCharacteristics: [QualityCharacteristic!]! @relationship(type: "RELEVANT_FOR", direction: OUT)
        sources: String!
    }
`;

// Create an instance of Neo4jGraphQL
const driver = neo4j.driver(
    "bolt://localhost:11003",
    neo4j.auth.basic("neo4j", "5499")
);

const neoSchema = new Neo4jGraphQL({ typeDefs, driver });


// Create an instance of ApolloServer
neoSchema.getSchema().then((schema) => {
    const server = new ApolloServer({
        schema,
    });

    server.listen({ port: 4019 }).then(({ url }) => {
        console.log(`🚀 Server ready at ${url}`);  // Where http://localhost:4019/ is the default URL which Apollo Server starts at.
    });
})

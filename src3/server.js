// https://neo4j.com/docs/graphql-manual/current/getting-started/
// Neo4j GraphQL Library

const { Neo4jGraphQL } = require("@neo4j/graphql");
const { ApolloServer, gql } = require("apollo-server");
const neo4j = require("neo4j-driver");

// define type definitions or schema
const typeDefs = gql`
    type Movie {
        title: String
        actors: [Actor!]! @relationship(type: "ACTED_IN", direction: IN)
    }

    type Actor {
        name: String
        movies: [Movie!]! @relationship(type: "ACTED_IN", direction: OUT)
    }
`;

// Create an instance of Neo4jGraphQL
const driver = neo4j.driver(
    "bolt://localhost:11003",
    neo4j.auth.basic("neo4j", "5499")
);

const neoSchema = new Neo4jGraphQL({ typeDefs, driver });


// Create an instance of ApolloServer
neoSchema.getSchema().then((schema) => {
    const server = new ApolloServer({
        schema,
    });

    server.listen({ port: 4019 }).then(({ url }) => {
        console.log(`🚀 Server ready at ${url}`);  // Where http://localhost:4019/ is the default URL which Apollo Server starts at.
    });
})
